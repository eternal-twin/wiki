#!/bin/bash
set -eux
chmod -R 755 build/
find build/ -type f -exec chmod 644 {} \;
chmod 755 build/bin/*.php
chmod 755 build/lib/scripts/jquery/update.sh
chmod 755 build/vendor/simplepie/simplepie/library/SimplePie.php
chmod 755 build/vendor/simplepie/simplepie/library/SimplePie/Registry.php
chmod 755 build/vendor/simplepie/simplepie/library/SimplePie/Cache/Memcached.php
chown -R http:http build
