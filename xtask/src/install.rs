use std::error::Error;
use std::fs;
use std::path::{PathBuf, Path};
use url::Url;
use flate2::read::GzDecoder;
use tar::Archive;

pub struct Dependencies {
  pub dir: PathBuf,
  pub tmp: PathBuf,
  pub tmp_dokuwiki: PathBuf,
  pub tmp_autotooltip: PathBuf,
  pub tmp_vshare: PathBuf,
  pub tmp_wrap: PathBuf,
  pub dokuwiki: PathBuf,
  pub autotooltip: PathBuf,
  pub vshare: PathBuf,
  pub wrap: PathBuf,
}

impl Dependencies {
  pub fn new() -> Self {
    let root = std::env::current_dir().unwrap();
    let dir: PathBuf = root.join("dependencies");
    let tmp: PathBuf = dir.join("tmp");
    let tmp_dokuwiki: PathBuf = tmp.join("dokuwiki.tgz");
    let tmp_autotooltip: PathBuf = tmp.join("autotooltip.tgz");
    let tmp_vshare: PathBuf = tmp.join("vshare.tgz");
    let tmp_wrap: PathBuf = tmp.join("wrap.tgz");
    let dokuwiki: PathBuf = dir.join("dokuwiki");
    let autotooltip: PathBuf = dir.join("autotooltip");
    let vshare: PathBuf = dir.join("vshare");
    let wrap: PathBuf = dir.join("wrap");
    Self {
      dir,
      tmp,
      tmp_dokuwiki,
      tmp_autotooltip,
      tmp_vshare,
      tmp_wrap,
      dokuwiki,
      autotooltip,
      vshare,
      wrap,
    }
  }
}

pub async fn install() -> Result<(), Box<dyn Error>> {
  let deps = Dependencies::new();

  eprintln!("Create dependencies/");
  if !deps.dir.exists() {
    fs::create_dir(&deps.dir).unwrap();
  }
  eprintln!("Create dependencies/tmp/");
  if !deps.tmp.exists() {
    fs::create_dir(&deps.tmp).unwrap();
  }

  eprintln!("Download dependencies/tmp/dokuwiki.tgz");
  download(Url::parse("https://github.com/splitbrain/dokuwiki/tarball/master").unwrap(), &deps.tmp_dokuwiki).await;
  eprintln!("Download dependencies/tmp/autotooltip.tgz");
  download(Url::parse("https://github.com/zioth/dokuwiki-autotooltip/tarball/master").unwrap(), &deps.tmp_autotooltip).await;
  eprintln!("Download dependencies/tmp/vshare.tgz");
  download(Url::parse("https://github.com/splitbrain/dokuwiki-plugin-vshare/tarball/master").unwrap(), &deps.tmp_vshare).await;
  eprintln!("Download dependencies/tmp/wrap.tgz");
  download(Url::parse("https://github.com/selfthinker/dokuwiki_plugin_wrap/tarball/master").unwrap(), &deps.tmp_wrap).await;
  eprintln!("Extract dependencies/dokuwiki");
  extract(&deps.tmp_dokuwiki, &deps.dokuwiki);
  eprintln!("Extract dependencies/autotooltip");
  extract(&deps.tmp_autotooltip, &deps.autotooltip);
  eprintln!("Extract dependencies/vshare");
  extract(&deps.tmp_vshare, &deps.vshare);
  eprintln!("Extract dependencies/wrap");
  extract(&deps.tmp_wrap, &deps.wrap);

  Ok(())
}

pub async fn download(src: Url, dest: &Path) {
  if !dest.exists() {
    let res = reqwest::get(src).await.unwrap();
    let content = res.bytes().await.unwrap();
    fs::write(&dest, &content).unwrap();
  }
}

pub fn extract(src: &Path, dest: &Path) {
  if !dest.exists() {
    let source = fs::File::open(&src).expect(&format!("Failed to open {}", src.display()));
    let tar = GzDecoder::new(source);
    let mut archive = Archive::new(tar);
    archive.unpack(&dest).expect(&format!("Failed to unpack from {} to {}", src.display(), dest.display()));
    let content: Vec<fs::DirEntry> = fs::read_dir(&dest).unwrap().map(|r| r.unwrap()).collect();
    if content.len() == 1 && content[0].file_type().unwrap().is_dir() {
      let sub_dir = content[0].path();
      for f in fs::read_dir(&sub_dir).unwrap().map(|r| r.unwrap()) {
        let p = f.path();
        fs::rename(&p, dest.join(p.file_name().unwrap())).unwrap();
      }
      fs::remove_dir(&sub_dir).unwrap();
    }
  }
}
