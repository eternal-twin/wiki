use clap::Parser;
use std::error::Error;

#[derive(Debug, Parser)]
#[clap(author = "Charles \"Demurgos\" Samborski")]
struct CliArgs {
  #[clap(subcommand)]
  task: Task,
}

#[derive(Debug, Parser)]
enum Task {
  #[clap(name = "build")]
  Build(BuildArgs),
  #[clap(name = "dump")]
  Dump(DumpArgs),
  #[clap(name = "install")]
  Install(InstallArgs),
  #[clap(name = "restore")]
  Restore(RestoreArgs),
}

#[derive(Debug, Parser)]
struct BuildArgs {}

#[derive(Debug, Parser)]
struct DumpArgs {}

#[derive(Debug, Parser)]
struct InstallArgs {}

#[derive(Debug, Parser)]
struct RestoreArgs {}

#[tokio::main]
async fn main() {
  let args: CliArgs = CliArgs::parse();

  let res = match &args.task {
    Task::Build(ref args) => build(args).await,
    Task::Dump(ref args) => dump(args).await,
    Task::Install(ref args) => install(args).await,
    Task::Restore(ref args) => restore(args).await,
  };

  match res {
    Ok(_) => std::process::exit(0),
    Err(_) => res.unwrap(),
  }
}

async fn build(_args: &BuildArgs) -> Result<(), Box<dyn Error>> {
  xtask::build().await
}

async fn dump(_args: &DumpArgs) -> Result<(), Box<dyn Error>> {
  xtask::dump().await
}

async fn install(_args: &InstallArgs) -> Result<(), Box<dyn Error>> {
  xtask::install().await
}

async fn restore(_args: &RestoreArgs) -> Result<(), Box<dyn Error>> {
  xtask::restore().await
}
