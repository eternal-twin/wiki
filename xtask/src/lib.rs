mod build;
mod config;
mod dump;
mod install;
mod restore;

pub use build::build;
pub use dump::dump;
pub use install::install;
pub use restore::restore;
use std::path::Path;
use std::fs;

pub fn copy_dir_rec(src: &Path, dest: &Path) {
  if !dest.exists() {
    fs::create_dir(&dest).unwrap();
  }
  for ent in fs::read_dir(&src).unwrap().map(|r| r.unwrap()) {
    let src_ent = ent.path();
    let dest_ent = dest.join(src_ent.file_name().unwrap());
    let ent_type = ent.file_type().unwrap();
    if ent_type.is_dir() {
      copy_dir_rec(&src_ent, &dest_ent);
    } else if ent_type.is_file() {
      fs::copy(&src_ent, &dest_ent).unwrap();
    } else {
      panic!("Unexpected file type");
    }
  }
}
