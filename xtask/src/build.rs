use std::error::Error;
use std::fs;
use std::path::PathBuf;
use crate::install::Dependencies;
use crate::copy_dir_rec;
use crate::config::{find_config, RawConfig};

pub struct Build {
  pub dir: PathBuf,
  pub src: PathBuf,
  pub deps: Dependencies,
}

impl Build {
  pub fn new() -> Self {
    let root = std::env::current_dir().unwrap();
    let dir: PathBuf = root.join("build");
    let src: PathBuf = root.join("src");
    Self {
      dir,
      src,
      deps: Dependencies::new(),
    }
  }
}

pub async fn build() -> Result<(), Box<dyn Error>> {
  let build = Build::new();
  let config = find_config(std::env::current_dir().unwrap()).unwrap();

  eprintln!("Copy dokuwiki");
  copy_dir_rec(&build.deps.dokuwiki, &build.dir);

  eprintln!("Copy authetwin plugin");
  copy_dir_rec(&build.src.join("plugins/authetwin"), &build.dir.join("lib/plugins/authetwin"));
  eprintln!("Copy autotooltip plugin");
  copy_dir_rec(&build.deps.autotooltip, &build.dir.join("lib/plugins/autotooltip"));
  eprintln!("Copy vshare plugin");
  copy_dir_rec(&build.deps.vshare, &build.dir.join("lib/plugins/vshare"));
  eprintln!("Copy wrap plugin");
  copy_dir_rec(&build.deps.wrap, &build.dir.join("lib/plugins/wrap"));

  eprintln!("Apply ACL");
  fs::copy(build.src.join("acl.auth.php"), build.dir.join("conf/acl.auth.php")).unwrap();

  eprintln!("Apply config");
  let config = get_config(&config);
  fs::write(build.dir.join("conf/local.php"), config).unwrap();

  eprintln!("Delete install.php");
  fs::remove_file(build.dir.join("install.php")).unwrap();

  Ok(())
}

pub fn get_config(conf: &RawConfig) -> String {
  format!(r#"<?php
$conf['title'] = 'Eternal Twinpedia';
$conf['license'] = '0';
$conf['basedir'] = '/';
$conf['dmode'] = 0775;
$conf['fmode'] = 0664;
$conf['useacl'] = 1;
$conf['autopasswd'] = 0;
$conf['rememberme'] = 0;
$conf['authtype'] = 'authetwin';
$conf['superuser'] = '@admin';
$conf['mailfrom'] = 'wiki@eternalfest.net';
$conf['userewrite'] = '1';
$conf['useslash'] = 1;
$conf['renderer_xhtml'] = 'autotooltip';
$conf['plugin']['smtp']['smtp_host'] = 'smtp.migadu.com';
$conf['plugin']['smtp']['smtp_port'] = '587';
$conf['plugin']['smtp']['smtp_ssl'] = 'tls';
$conf['plugin']['smtp']['auth_user'] = 'wiki@eternalfest.net';
$conf['plugin']['smtp']['auth_pass'] = '{smtp}';
$conf['plugin']['smtp']['debug'] = '1';
$conf['plugin']['authetwin']['etwin_uri'] = '{etwin_uri}';
$conf['plugin']['authetwin']['callback_uri'] = '{external_uri}/oauth/callback';
$conf['plugin']['authetwin']['client_id'] = '{etwin_client_id}';
$conf['plugin']['authetwin']['client_secret'] = '{etwin_client_secret}';
"#,
          smtp = &conf.smtp_key,
          etwin_uri = &conf.etwin_uri,
          external_uri = &conf.external_uri,
          etwin_client_id = &conf.etwin_client_id,
          etwin_client_secret = &conf.etwin_client_secret,
  )
}
