use std::error::Error;
use std::fs;
use std::path::PathBuf;
use crate::copy_dir_rec;

pub struct Dirs {
  pub build: PathBuf,
  pub snapshot: PathBuf,
}

impl Dirs {
  pub fn new() -> Self {
    let root = std::env::current_dir().unwrap();
    let build: PathBuf = root.join("build");
    let snapshot: PathBuf = root.join("snapshot");
    Self {
      build,
      snapshot,
    }
  }
}

pub async fn restore() -> Result<(), Box<dyn Error>> {
  let dirs = Dirs::new();

  const DIRS: [&str; 6] = [
    "pages",
    "meta",
    "media",
    "media_meta",
    "attic",
    "media_attic",
  ];

  for dir in DIRS {
    eprintln!("Restoring {}", dir);
    if dirs.snapshot.join(dir).exists() {
      if dirs.build.join("data").join(dir).exists() {
        fs::remove_dir_all(dirs.build.join("data").join(dir)).unwrap();
      }
      copy_dir_rec(&dirs.snapshot.join(dir), &dirs.build.join("data").join(dir));
    }
  }

  Ok(())
}
