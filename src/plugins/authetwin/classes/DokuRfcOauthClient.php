<?php

namespace Etwin\OauthClient;

final class DokuRfcOauthClient {
    /** @var string */
    private $authorizationEndpoint;
    /** @var string */
    private $tokenEndpoint;
    /** @var string */
    private $callbackEndpoint;
    /** @var string */
    private $clientId;
    /** @var string */
    private $clientSecret;

    /** @var \dokuwiki\HTTP\DokuHTTPClient  */
    private $client;

    /**
     * DokuRfcOauthClient constructor.
     *
     * @param string $authorizationEndpoint
     * @param string $tokenEndpoint
     * @param string $callbackEndpoint
     * @param string $clientId
     * @param string $clientSecret
     */
    final public function __construct(
        $authorizationEndpoint,
        $tokenEndpoint,
        $callbackEndpoint,
        $clientId,
        $clientSecret
    ) {
        $this->authorizationEndpoint = $authorizationEndpoint;
        $this->tokenEndpoint = $tokenEndpoint;
        $this->callbackEndpoint = $callbackEndpoint;
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
        $this->client = new \dokuwiki\HTTP\DokuHTTPClient();
    }

    /**
     * @param string $scope Scope string.
     * @param string $state Client state.
     * @return string Authorization URI where the user should be redirected.
     */
    final public function getAuthorizationUri($scope, $state) {
        $qs = implode("&",             [
            "access_type=offline",
            "response_type=code",
            "redirect_uri=" . urlencode($this->callbackEndpoint),
            "client_id=" . urlencode($this->clientId),
            "scope=" . urlencode($scope),
            "state=" . urlencode($state),
        ]);

        return $this->authorizationEndpoint . '?' . $qs;
    }

    /**
     * @param string $code One-time authorization code.
     * @return mixed OAuth access token.
     */
    final public function getAccessTokenSync($code) {
        $this->client->headers["Authorization"] = $this->getAuthorizationHeader();
        $raw = $this->client->post(
            $this->tokenEndpoint,
            [
                "client_id" => $this->clientId,
                "client_secret" => $this->clientSecret,
                "code" => $code,
                "grant_type" => "authorization_code",
            ]
        );
        return json_decode($raw);
    }

    /**
     * @return string
     */
    final private function getAuthorizationHeader() {
        return "Basic " . base64_encode($this->clientId . ":" . $this->clientSecret);
    }
}
