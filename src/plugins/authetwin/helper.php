<?php
/**
 * DokuWiki Plugin authetwin (Helper Component)
 */

// must be run within Dokuwiki
if(!defined('DOKU_INC')) die();

class helper_plugin_authetwin extends DokuWiki_Plugin {
    /**
     * Load the needed libraries and initialize the Oauth client.
     *
     * @return \Etwin\OauthClient\DokuRfcOauthClient
     */
    public function loadOauthClient() {
        global $conf;

        require_once(__DIR__.'/classes/DokuRfcOauthClient.php');

        $client = new \Etwin\OauthClient\DokuRfcOauthClient(
            $conf['plugin']['authetwin']['etwin_uri'] . '/oauth/authorize',
            $conf['plugin']['authetwin']['etwin_uri'] . '/oauth/token',
            $conf['plugin']['authetwin']['callback_uri'],
            $conf['plugin']['authetwin']['client_id'],
            $conf['plugin']['authetwin']['client_secret']
        );

        return $client;
    }

    /**
     * Load the needed libraries and initialize the Etwin client.
     *
     * @return \Etwin\OauthClient\DokuEtwinClient
     */
    public function loadEtwinClient() {
        global $conf;

        require_once(__DIR__.'/classes/DokuEtwinClient.php');

        $client = new \Etwin\OauthClient\DokuEtwinClient(
            $conf['plugin']['authetwin']['etwin_uri']
        );

        return $client;
    }
}
