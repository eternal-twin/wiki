# Eternal-Twin Wiki

## Quickstart

```
# Install the base dependencies
cargo xtask install
# Build the Wiki
cargo xtask build
# Remove the old build and do a new build
./rebuild.sh
# Export data, to create a backup
cargo xtask dump
# Impor data, from a backup
cargo xtask restore
# Install a local Eternaltwin
yarn install
# Configure the local Eternaltwin
cp ./etwin.toml.example ./etwin.toml
# Start a local Eternlatwin version
yarn eternaltwin start
```

## Structure

The Wiki is created by downloading [the latest Dokuwiki release](https://github.com/splitbrain/dokuwiki)
with its dependencies and merging custom changes and config.
The build step copies the content of `./src` and generates the config files
`conf/acl.auth.php` and `conf/local.php`.
